CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

This module provides a video embed field provider that allows you to embed
videos from Bliverr.

REQUIREMENTS
------------

This module requires the following modules:
 * Video Embed Field (https://www.drupal.org/project/video_embed_field)

INSTALLATION
------------

 * Install and enable this module like any other drupal 8 module.

CONFIGURATION
-------------

 * Enable the Video Embed Bliverr module on your main site.
 * To be able to add a new Bliverr video, you must add the Bliverr provider in
   the provider list of your video field.

MAINTAINERS
-----------

Current maintainers:
 * Jérémy Rambaud (Jérémy Rambaud) - https://www.drupal.org/user/3549289

This project has been sponsored by:
 * Smile - http://www.smile.fr
